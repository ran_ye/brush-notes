### SQL1 查找最晚入职员工的所有信息

题目[链接](https://www.nowcoder.com/practice/218ae58dfdcd4af195fff264e062138f?tpId=82&&tqId=29753&rp=1&ru=/ta/sql&qru=/ta/sql/question-ranking)。

**解题关键：**子查询。不能使用limit, offset，因为当天入职的人数不确定，limit后面显示的记录数也无法确定。

```mysql
select * from employees where hire_date = (select max(hire_date) from employees);
```



### SQL2 查找入职员工时间排名倒数第三的员工所有信息

题目[链接](https://www.nowcoder.com/practice/ec1ca44c62c14ceb990c3c40def1ec6c?tpId=82&&tqId=29754&rp=1&ru=/ta/sql&qru=/ta/sql/question-ranking)。

**解题关键：**子查询。注意子查询中的去重。

```mysql
select * from employees where hire_date = 
(select distinct hire_date from employees order by hire_date desc limit 1 offset 2);
```



### SQL8 找出所有员工当前薪水salary情况

题目[链接](https://www.nowcoder.com/practice/ae51e6d057c94f6d891735a48d1c2397?tpId=82&&tqId=29760&rp=1&ru=/activity/oj&qru=/ta/sql/question-ranking)。

```mysql
select distinct salary from salaries order by salary desc;
```

